package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	Title       string             `bson:"Title,omitempty"`
	Description string             `bson:"Description,omitempty"`
}
