package service

import (
	"context"
	"gRPC_Example/internal/core"
	"gRPC_Example/proto"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ArticleRepository interface {
	GetById(ctx context.Context, id string) (*core.User, error)
}

type ArticleService struct {
	proto.UserServiceServer
	userRepository ArticleRepository
}

func NewUserService(userRepository ArticleRepository) *ArticleService {
	return &ArticleService{
		articleRepository: ArticleRepository,
	}
}

func (service *ArticleService) GetUser(ctx context.Context, request *proto.ArticleRequest) (response *proto.UserResponse, err error) {
	user, err := service.userRepository.GetById(ctx, request.GetId())

	if err != nil {
		return nil, err
	}

	if user == nil {
		user = &core.User{
			ID:          primitive.ObjectID{},
			Title:       "",
			Description: "",
		}
	}

	return &proto.UserResponse{Name: article.title}, nil
}
